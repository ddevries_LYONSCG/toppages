// ==UserScript==
// @name         Top Pages
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  shows how to use babel compiler
// @author       You
// @match        *://*/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.18.2/babel.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.16.0/polyfill.js
// @require      https://code.jquery.com/jquery-3.2.1.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/ScrollToFixed/1.0.8/jquery-scrolltofixed-min.js
// @require      file:///Users/ddevries/Desktop/DEV%20Source/LyonsU/tampermonkey/topPages/topPages.js
// @resource     Bootstrap https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css
// @resource     https://fonts.googleapis.com/css?family=Exo+2
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @run-at       document-end
// @noframes
// ==/UserScript==

/* jshint ignore:start */
var inline_src = (<><![CDATA[
/* jshint ignore:end */
    /* jshint esnext: false */
    /* jshint esversion: 6 */

// Globals
var KEY_ID = "<TM>";

// Top Site object
class TopSite {
    constructor(_url, _title) {
        this.url = _url;
        this.title = _title;
        this.lastVisited = Date.now();
        this.count = 1;
    }
}

// Helpers
function checkKey(key) {
    if (key.slice(0, 4) === KEY_ID) {
        return true;
    } else {
        return false;
    }
}

(function () {
    'use strict';
    // Add new top site or update existing
    let currentUrl = window.location.href;
    let key = KEY_ID + currentUrl;
    let topSite = JSON.parse(localStorage.getItem(key));
    if (!topSite) {
        topSite = new TopSite(currentUrl, $(document).prop('title'));
        localStorage.setItem(key, JSON.stringify(topSite));
    } else {
        topSite.count++;
        topSite.lastVisited = Date.now();
        localStorage.setItem(key, JSON.stringify(topSite));
    }

    // Create local array of top sites and sort them based on count
    let topSites = [];
    for (let i = 0; i < localStorage.length; ++i) {
        let key = localStorage.key(i);
        if (checkKey(key)) {
            topSites.push(JSON.parse(localStorage.getItem(key)));
        }
    }
    topSites.sort((a, b) => b.count - a.count);

    // DOM is ready manipulation
    $(document).ready(function () {
        // transition state flag
        let closed = true;

        // add and style topSites container
        $('body').before("<div class='topSites'></div>");
        $('.topSites').css({
            "width"          : "100%",
            "min-width"      : "800px",
            "height"         : "150px",
            "display"        : "flex",
            "flex-direction" : "row",
            "align-items"    : "center",
            "position"       : "fixed",
            "overflow-x"     : "auto",
            "flex-wrap"      : "nowrap",
            "box-shadow"     : "inset 0px 0px 30px 1px rgba(0,0,0,0.5)",

        });

        // add topSite containers
        for (let i = 0; i < topSites.length; ++i) {
            $('.topSites').append(`<div class='topSite' style='padding:10px; flex: 0 0 25%; self-align:center; text-align:center'>
            <p style='margin-bottom: 5px; font-size: 14px'><a href='${topSites[i].url}' > '${topSites[i].title}'</a></p>
            <p style='margin-bottom: 5px; font-size: 12px'>Count: ${topSites[i].count}</p>
            <p style='margin-bottom: 5px; font-size: 12px'>Last Visted: ${new Date(topSites[i].lastVisited).toLocaleString()}</p>
            </div>`);
            if (topSites.length > 1) {
                $('.topSites').append(`<hr></hr>`);
            }
        }
        
        // style spacers
        $('hr').css({
            "height" : "80%",
            "border": "1px solid lightgrey",
            "margin-bottom": "5px",
            "box-shadow"     : "0px 0px 20px 1px rgba(100,100,100,0.25)",
        });

        // wrap body; enables translation of entire page
        $('body').wrap("<div style='height: 100%; width: 100%; visibility: visible !important' class='bodyWrapper'></div>");
        
        // transition/animation
        $('.bodyWrapper').css({
            "-webkit-transition" : "transform 1s ease",
            "transition" : "transform 1s ease",
            "-webkit-transform" : "translateY(0px)",
            "transform"         : "translateY(0px)",
        });
        $('.topSite').css({
            "-webkit-transition" : "opacity 1.5s ease-out",
            "transition" : "opacity 1.5s ease-out",
            "opacity"    : "0",
        });

        // event handler
        $(document).keypress(function(e) {
            if (e.key === 't' && (closed)) {
                closed = false;
                $('.bodyWrapper').css({
                    "-webkit-transform" : "translateY(150px)",
                    "transform"         : "translateY(150px)"
                });
                $('.topSite').css({
                    "opacity" : "1"
                });
                
            } else {
                
                if (e.key === 't') {
                    closed = true;
                    $('.bodyWrapper').css({
                        "-webkit-transform" : "translateY(0px)",
                        "transform"         : "translateY(0px)"
                    });
                    $('.topSite').css({
                        "opacity" : "0"
                    });
                }
            }
        });
    });
})();

/* jshint ignore:start */
]]></>).toString();
var c = Babel.transform(inline_src, { presets: [ "es2015", "es2016" ] });
eval(c.code);
/* jshint ignore:end */